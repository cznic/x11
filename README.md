# X11

CGo-free port of libx11, the Core X11 protocol client library.


## Installation

    $ go get modernc.org/x11

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/x11/lib

## Documentation

[godoc.org/modernc.org/x11](http://godoc.org/modernc.org/x11)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fx11](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fx11)
